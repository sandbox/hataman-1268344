<?php

function bluevis_user_bar() {
  global $user;                                                               
  $output = '';

  if (!$user->uid) { 
    $output .=('<h2>User Login</h2>');  
    $output .= drupal_get_form('user_login_block');                           
  }                                                                           
  else {    
    $output .= t('<h2>!user</h2>', array('!user' => theme('username', $user)));	
    $output .= theme('item_list', array(
	  l(t('Track'), 'user/'.$user->uid .'/track', array('title' => t('Your track'))),
      l(t('Your account'), 'user/'.$user->uid , array('title' => t('Edit your account'))),
      l(t('Sign out'), 'logout')));
  }
  $output = '<div id="user-login-block" class="bloks">'.$output.'</div>';
     
  return $output;
}

function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
      $breadcrumb[] = drupal_get_title();
        array_shift($breadcrumb);
       return '<div class="bread">'. implode(' <span clas="sep"> >> </span> ', $breadcrumb) .'</div>';
  }
  }
 
function bluevis_preprocess_search_theme_form(&$vars, $hook) {
  // Remove the "Search this site" label from the form.
  $vars['form']['search_theme_form']['#title'] = t('');
 
  // Set a default value for text inside the search box field.
  $vars['form']['search_theme_form']['#value'] = t('Search this Site');
 
  // Add a custom class and placeholder text to the search box.
  $vars['form']['search_theme_form']['#attributes'] = array('class' => 'hataman_search', 'onblur' => "if (this.value == '') {this.value = '".$vars['form']['search_theme_form']['#value']."';} ;", 'onfocus' => "if (this.value == '".$vars['form']['search_theme_form']['#value']."') {this.value = '';} ;" );

 
  // Change the text on the submit button
  //$vars['form']['submit']['#value'] = t('Go');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

  $vars['form']['submit']['#type'] = 'button';
  $vars['form']['submit']['#id'] = 'hataman_search';
   
  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}



?>