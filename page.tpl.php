<?php
// $Id: page.tpl.php,v 1.18.2.1 2009/04/30 00:13:31 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
</head>
<body>
<div id="bluevis">
    <div id="header" class="left">
	    <div id="sites" class="left"><?php
		   if($logo){
		     print '<a href="'.check_url($front_page).'" id="logo" class="left"><img src="'.check_url($logo).'" alt="" /></a>';
		   }
		   if($site_name){
		     print '<a href="'.check_url($front_page).'" class="sitename left">'.$site_name.'</a>';
		   }
		   if($site_slogan){
		     print '<a href="'.check_url($front_page).'" class="site-slogan left">'. $site_slogan .'</a>';
		   }
		?>
		</div>
		<div id="arama"><?php print $search_box ?></div>
	</div>
		<?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'ana_linkler')) ?>
        <?php endif; ?>
	<div id="main">
	    <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'secondary_linkler')) ?>
        <?php endif; ?>

	    <div class="main_top" class="left"></div>
		<div class="main_main" class="left">
	    <div id="left" class="left sidebar">
		    <?php print bluevis_user_bar() ?>
			<?php print $left ?>
		</div>
		<div id="right" class="left">
          <div class="cont">
		  <?php print $breadcrumb; ?>
		  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<div id="baslik">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="icerikbaslik"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <?php print $content ?></div>
          <div class="temz"></div>	
        </div>	
		<div id="footer" class="left">
		    <?php print $footer_message . $footer ?>
		</div>
		<div class="temz"></div>
		</div>
		<div class="main_bottom" class="left"></div>
		<div class="temz"></div>
	</div>	
<div class="temz"></div>	
</div>
  <?php print $closure ?>
  </body>
</html>
