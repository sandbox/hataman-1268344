<?php
// $Id: block.tpl.php,v 1.3 2007/08/07 08:39:36 goba Exp $
?>
<div class="bloks">
<div id="<?php print $block->module .'-'. $block->delta; ?>" class="<?php print $block->module ?>">

<?php if (!empty($block->subject)): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>

<div class="blok_content"><?php print $block->content ?></div>
</div>
</div>
